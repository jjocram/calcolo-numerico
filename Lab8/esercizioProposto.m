f = @(x) exp(x-2).*sin(x);
a = 1;
b = 3;

I_vera = quad(f, a, b, 1e-14)
E_trap_all = [];
E_simpson_all = [];

fid = fopen('tabEsProposto.ris', 'w');
fprintf(fid, ' \n nval I_Trap Err_Trap  I_Simp Err_simp');
fprintf(' \n nval I_Trap Err_Trap  I_Simp Err_simp');

nvals = 11:10:201;
for nval = 11:10:201
    m = nval-1;
    
    N_trap = m;
    [x_trap,w_trap, I_trap]=trapezi_composta(N_trap,a,b,f);
    
    N_simpson = m/2;
    [x_simp,w_simp, I_simp]=simpson_composta(N_simpson,a,b,f);
    
    E_trap = abs(I_vera - I_trap);
    E_trap_all = [E_trap_all, E_trap];
    
    E_simpson = abs(I_vera - I_simp);
    E_simpson_all = [E_simpson_all, E_simpson];
    
    %Stampa su console
    fprintf(' \n %7d %20.14f %12.3E  %18.14f %13.3E',nval,I_trap,E_trap/I_vera,I_simp,E_simpson/I_vera);
    fprintf(' \n');
    
    %Stampa su file
    fprintf(fid, ' \n %7d %20.14f %12.3E  %18.14f %13.3E',nval,I_trap,E_trap/I_vera,I_simp,E_simpson/I_vera);
    fprintf(fid, ' \n');
end
fclose(fid);

%Disegno i grafici degli errori relativi alle formule composte in funzione
%di h
loglog(nvals, E_trap_all);
hold on 
loglog(nvals, E_simpson_all);
hold off
