function [ x, w, ITrap ] = simpson_composta( N, a, b, f)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    h = (b-a)/(2*N);
    x = a:h:b; %a...x1...x2...x3...x4...||...x(n-1)...b dove ...= distanza h/2
    x = x';
    w = ones(2*N+1, 1); %dove 2*N+1 e' il numero di nodi di x
    w(2:2:2*N, 1) = 4;
    w(3:2:2*N-1, 1) = 2;
    w = w*h/3;

    fx = feval(f, x);
    ITrap = w' * fx;
end

