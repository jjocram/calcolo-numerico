function [ L, U ] = lugauss( A )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
n = size(A, 1);
L = eye(n);

for k=1:n-1
   for i=k+1:n
       L(i,k) = A(i,k)/A(k,k);
       for j=k:n
          A(i,j) = A(i,j) - (L(i,k)*A(k,j)); 
       end
   end
end

U = A;
end

