function u = hornerN( x, c, xStar )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    n1 = length(x);
    if (n1 ~= length(c))
        error('Errore: lunghezza tra i vettori x e c non uguale')
    else
       u = c(n1);
       for j = n1-1:-1:1 %da n1-1 a 1 "incrementando" di -1
            u = u.*(xStar-x(j)) + c(j);
       end
    end
end

