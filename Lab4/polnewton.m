function c = polnewton( x, y )
%polnewton calcola i coefficienti del polinomio interpolatore utilizzando
%la forma di Newton con le differenze divise
%Uso:
%   c = polnewton(x,y)
%Input:
%   x: vettore dei nodi
%   y: vettore dei valori della funzione da interpolare nei nodi 
%Output;
%   c: vettore colonna dei coefficienti ordinati per indici crescenti
    ctab = [];
    n1 = length(x);
    if (n1 ~= length(y))
        beep
        error('Errore: lunghezza vettori x e y non uguale')
    else
        for i = 1:n1
            ctab(i, 1) = y(i);
        end
        
        for j = 2:n1
            for i = 1:n1-j+1
                ctab(i,j) = (ctab(i+1, j-1) - ctab(i, j-1))/(x(i+j-1) - x(i));
            end
        end
        c = ctab(1,:); %prima riga della matrice 
    end
end

