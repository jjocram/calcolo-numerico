function [ t ] = interpol( x, y, s )
%Interpoolazione polinomiale
%   x:
%   y:
%   s:
% 
%   t:

    grado = length(x) - 1;
    coeff = polyfi(x,y,grado);
    t = polyval(coeff, s);
end

