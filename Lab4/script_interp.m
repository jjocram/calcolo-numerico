%===================================================
% S c r i p t p e r i n t e r p o l a z i o n e con l a f o r m a d i Newton
% N e c e s s i t a d e l l e f u n c t i o n polnewton e hornerN
%===================================================
% Definisce la funzione
f_string = '1./(1+x.^2)';
f = inline(f_string);
% Definisce l ’ intervallo [a ,b]
a = -5;
b = 5;
% Numero di nodi
%n = 11;
fid = fopen('risequis.txt', 'w');
for n = 1:31

x = linspace(a, b , n);
y = f(x);
xVal = linspace(a, b, 201);

c = polnewton(x,y);
yVal = [];
for i = 1:length(xVal)
   yVal = [yVal hornerN(x, c, xVal(i))]; 
end

errore = f(xVal) - yVal;
normaErrore = norm(errore, 'inf');

fprintf(fid, '%d\t%d\n', n, normaErrore);

end
fclose(fid);
%plot(xVal, f(xVal), 'b-');
%hold on
%plot (xVal, yVal, 'r--');
%plot(x,y, 'ro');
%hold off