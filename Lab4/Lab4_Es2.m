% Esercizio d'esempio per l'interpolazione
n = input('Introduci il numero di punti: ');
a = -5;
b = 5;

% Calcoliamo n nodi equispaziati
x = linspace(a, b, n);
% Calcoliamo le ordinate dei punti da interopolare sulla funzione sin(x)
y = sin(x);

% Vettore xVal di 201 punti
xVal = linspace(a, b, 201);

% Calcolo del polinomio con polyfit
grado = n-1;
c = polyfit(x, y, grado);

% Valuto con polyval i coefficienti che stanno in c 
yVal = polyval(c, xVal);

% Rappresentazione grafica
plot(xVal, sin(xVal), 'b-');
hold on
plot(xVal, yVal, 'r--');
plot(x, y, 'ro');
hold off;
string = ['sin(x) e il suo interpolante di grado ', num2str(grado)];
title(string);
legend('sin(x)', 'interpolante', 'punti di interpolazione');