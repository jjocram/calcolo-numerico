% Consegna di Disegna_Base.m
% Disegnare nell'intervallo [-5;5] la funzione x^2 - 2e^x/x colcolandone il
% valore su diverse quantit e vedendo come cambia il grafico

a = -5;
b = 5;
%funs = 'x.^2-2*exp(x)./x'; %La funzione va definita come stringa; usando
%le anonymous function questo non e' necessario. Comunque, notare il .^ che
%rende l'operazione vettoriale
fun = @(x)x.^2-2*exp(x)./x; %inline(funs); %inline verra deprecato; guardarsi le anonymous function (questa con la @)
n = 101; %Numero di punti che verranno usati
x = linspace(a,b,n); %linspace(inizio,fine,numero di punti-1)
y = fun(x);
plot(x,y)