a = input('Dammi l''estremo sinistro: ');
b = input('Dammi l''estremo destro: ');
fun_s = input('Dammi la stringa funzione: ');
n = input('Dammi il numero di punti: ');

disp('Estremo sinistro'); disp(a)
disp('Estremo destro'); disp(b)
disp('Numero di punti'); disp(n)

fun = inline(fun_s);
disp('Funzione'); disp(fun);

x = linspace(a, b, n);
y = fun(x);

plot(x,y);
hold on
plot([a,b],[0,0],'k-')
hold off