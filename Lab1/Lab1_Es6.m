g = @(x) exp(-2.*x) + x.^2 - 0.181734;
f = @(x) x;

n = 1000;

x = linspace(0, 10, n);
y = g(x);
y2 = f(x);

hold on
plot(x, y2, 'r');
plot(x, y, 'g');
ylim([-10;10])
hold off

%L'unico punto di intersezione tra g(x) e la bisettrice in cui si pu�
%convergere applicando il metodo di punto fisso � quelo che si trova
%nell'intervallo [0.42;0.44].
%Dal disegno del grafico si pu� notare che g'(x) calcolato nel punto di
%intersezione compreso nell'intervallo [0.42;0.44] � minore di 1 a
%differenza dell'altro che � maggiore di 1. Il fatto che la derivata prima
%sia minore di uno � la condizione necessaria affinche il metodo del punto
%fisso converga ad una soluzione.