a = input('Dammi l''estremo sinistro: ');
b = input('Dammi l''estremo destro: ');
fun1_s = input('Dammi la stringa funzione 1: ');
fun2_s = input('Dammi la stringa funzione 2: ');
n = input('Dammi il numero di punti: ');

disp('Estremo sinistro'); disp(a)
disp('Estremo destro'); disp(b)
disp('Numero di punti'); disp(n)

%fun_s = strcat(fun2_s, '-', fun1_s);

%fun = inline(fun_s);
fun1 = inline(fun1_s);
fun2 = inline(fun2_s);
disp('Funzione1'); disp(fun1);
disp('Funzione2'); disp(fun2);

x = linspace(a, b, n);
%y = fun(x);
y1 = fun1(x);
y2 = fun2(x);

hold on
title("Marco Ferrati 1168234")
%plot(x, y, 'b-');
plot(x,y1, 'r-');
plot(x, y2, 'g-');
plot([a,b],[0,0],'k-')
hold off

print( '?f1' , 'Grafico_disegna2' , '?dpdf')