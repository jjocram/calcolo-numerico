function [ xv, fxv, n, flag ] = metodoDiHalley( f, f1, f2, x0, toll, nmax )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
xv = x0;
fxv = f(x0);
n = 1;
flag = 0;

err = toll+1;
while (n<nmax) && (err>toll) && (flag == 0)
    xnPrev = xv(n);
    
    num = (2*f(xnPrev)*f1(xnPrev));
    denom = (2*f1(xnPrev)*f1(xnPrev) - f(xnPrev)*f2(xnPrev));
    
    if (denom ~= 0)
        xnNext = xnPrev - num/denom;
    
        err = abs(xnNext-xnPrev);
        xv = [xv xnNext];
        fxv = [fxv f(xnNext)];
        n = n+1;
    else
        flag = 1;
    end

end
   if (n == nmax)
      flag = 2; 
   end
end

