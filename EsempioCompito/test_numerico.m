x0 = 0.5;
toll = 10^(-10);
nmax = 50; %non c'e' scritto nella consegna

f_string = 'x^2 - 1 + exp(-x)';
f1_string = '2*x -exp(-x)';
f2_string = '2 + exp(-x)';
f = inline(f_string);
f1 = inline(f1_string);
f2 = inline(f2_string);

[xv, fxv, n, flag] = metodoDiHalley(f, f1, f2, x0, toll, nmax);

switch flag
    case 0
        fprintf('Iter\txn\t|f(xn)|\n') 
        for i=1:n
            fprintf('%d\t%e\t%e\n', i, xv(i), abs(fxv(i)));
        end;
        s = [];
        fprintf('|x(n)-x(n-1)|\n')
        for i=2:n
            s = [s abs(xv(i)-xv(i-1))];
            fprintf('%g\n', s(i-1)); 
        end
        semilogy(0:n-2, s, 'or')
        hold on
        semilogy(0:n-2, s, '-k');
        hold off
    case 1
        fprintf('Il denominatore nell iterata %d si e annullato', n+1)
    case 2
        fprintf('Il numero di iterate massime e stato superato')        
end     
    
