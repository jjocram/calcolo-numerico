function [xv, scarti, iter] = secvar(f, x0, x1, toll, nmax)
%SECVAR Metodo della secante variabile per equazione non lineare
%
% Uso:
%   [xv, scarti, n] = secvar(f, x0, x1, toll, nmax)
%
% Dati di ingresso:
%   f: funzione
%   x0: prima iterata
%   x1: seconda iterata
%   toll: tolleranza richiesta per il valore assoluto tra due iterate successive (scarto)
%   nmax: massimo numero di iterate permesse
%
% Dati di uscita:
%   xv:     vettore contenente le iterate
%   scarti: vettore contenente i corrispondenti scarti
%   n:      numero di iterate della successione
    xv = [x0 x1];
    scarti = x1-x0;
    iter = 0;
    flag = 0;
    
    diff = x1-x0;
    while(abs(diff) >= toll && (iter<nmax) && (flag == 0))
        h = (f(xv(end)) - f(xv(end-1)))/(xv(end) - xv(end-1));
        X = xv(end) - f(xv(end))/h;
        
        xv = [xv X];  
        diff = xv(end) - xv(end-1);
        scarti = [scarti diff];
        iter = iter + 1;
    end
end

