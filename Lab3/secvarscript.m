exprf = string(strsplit(input('Stringa nella forma funzione x0 x1 tolleranza numeroMassimoDiIterazioni: ')));

f = inline(exprf(1));
x0 = str2double(exprf(2));
x1 = str2double(exprf(3));

[xv, scarti, iter] = secvar(f, x0, x1, str2num(exprf(4)), str2num(exprf(5)));
if (iter == str2num(exprf(5)))
    disp('Sono state raggiunte il numero massimo di iterazioni')
else
    %Stampa a video dei valore di secvar
    file = fopen('tabellaRisultati.txt','w')
    fprintf(file,'Iterata \tXi \t Scarto \n')
    fprintf(file,'0 \t%20.15f \t -- \n', xv(1))
    fprintf(file,'%d \t%20.15f \t %10.2e \n', [1:length(scarti); xv(2:end); scarti]);
fclose(file);
    %Grafico della funzione passata
    figure(1)
    fplot(f);
    hold on; 
    plot([-10 10],[0 0],'k-');
    title('FERRATI MARCO 1168234');
    hold off;
    print( 'funzione' , '-dpdf')

    % Grafico dei valori assoluti degli scarti
    figure(2)
    xiter=0:iter;
    semilogy(xiter,abs(scarti))
    title('FERRATI MARCO 1168234');
    print('scarti' , '-dpdf')
end
