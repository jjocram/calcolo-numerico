function [ xv, scarti, iter, flag ] = newtonfun( f, df, x0, toll, nmax )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
xv = x0;
scarti = [];
iter = 0;
flag = 0;

diff = toll+1;

x = x0;

while(abs(diff) >= toll && (iter<nmax) && (flag == 0))
    fx = f(x);
    dfx = df(x);
    
    if (dfx == 0)
        flag = 1;
    else
       diff = -fx/dfx;
       x = x + diff;
       fx = f(x);
       xv = [xv; x];
       scarti = [scarti; diff];
       iter = iter+1;
    end
end

end

