%Ferrati Marco 1168234
f_string = '5.*x - exp(-x)';
f = inline(f_string);

tolleranza = 10^(-8);
nMax = 100;
a = -1;
b = 1;

%Disegno il grafico
x = linspace(-2,2,101);
y = f(x);
plot(x,y)
hold on
plot([-3,3], [0,0], 'k-');

%Eseguo la funzione bisezione
[vc,sl,vr,iter]=bisezione(f,a,b,tolleranza,nMax);
%Stampa dei risultati mediante un ciclo for
fprintf('n\txn\tF(xn)\tb_n-a_n\n')
for k=1:iter
    fprintf('\n%3.0f\t%15.15f\t%5.2e\t%5.2e  ',k,vc(k),vr(k),sl(k))
end
fprintf('\n');

figure
semilogy(0:iter-1, vc)
title('Ferrati Marco 1168234')