%Ferrati Marco 1168234
function [ xv, slv, fxv, n ] = bisezione( f, a, b, toll, nmax )
%UNTITLED2 Summary of this function goes here
%   USO: [ xv, slv, fxv, n ] = bisezione( f, a, b, toll, nmax )
%INPUT:
%   f: funzione (inline function)
%   a: estremo sinistro
%   b: estremo destro
%   toll: tolleranza richiesta per il test d'ingresso
%   nmax: massimo indice dell'iterata permesso
%
%OUTPU:
%   xv: vettore contenente l'iterata
%   slv: vettore contenente le seilunghezze degli intervalli
%   fxv: vettore contenente i corrispondneti residui
%   n: indice dell'iterata finale calcolata

xv = [];
slv = [];
fxv = [];

for index = 1:nmax
   c = (a+b)/2;
   fc = f(c);
   
   semilun = (b-a)/2;
   res = abs(fc);
   
   xv = [xv; c];
   slv = [slv; semilun];
   fxv = [fxv, res];
   
   if(res < toll) || (semilun < toll)
       n=index;
       return;
   end
   
   if sign(fc) == sign(f(a))
      a=c;
   else
       b=c;
   end
end

n = index;

end

