function [ x1,x2,err] = Lab2_Esempio1( a,b,c ) %NB: la funzione deve avere lo stesso nome del file .m
%Calcola le radici di un'equazione di secondo grado
%   USO (come chimare questa funzione): [x1,x2,err] = Lab2_Esempio1(a,b,c)
err = 0;
delta = b^2-4*a*c;
if delta < 0
    disp('Radici complesse')
    x1=0;x2=0;err=1;
else
    x1 = (-b-sqrt(delta))/(2*a);
    x2 = (-b+sqrt(delta))/(2*a);
end