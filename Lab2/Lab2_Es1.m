f_string = 'exp(-x)';
f1_string = 'x.*5';

f = inline(f_string);
f1 = inline(f1_string);

toll = 10^(-8);

x = linspace(-10,10,1001);
y = f(x);
y1 = f1(x);
semilogy(x,y)
hold on