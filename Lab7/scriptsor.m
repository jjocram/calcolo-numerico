n = 5;
v = 2*ones(n, 1)
t = -ones(n-1, 1)

A = diag(v) + diag(t, -1) + diag(t, +1) %Creo la matrice aggiundendo le diagonali
n=size(A,1);
b = A*ones(n,1);

%Parametri di ingresso della function jacobi
toll = 1e-8;
itmax= 200;
x0=zeros(n,1);

%Chiamata alla function jacobi
[x,itersor,normscartijac]=sor(A,b,x0,itmax,toll, 1.0);
itersor
x

% Grafico di convergenza
semilogy(1:itersor,normscartijac,'bo-','linewidth',2);
legend('Jacobi');
xlabel('iterazioni');ylabel('||scarto||')
title('Profilo di convergenza del  metodo SOR')