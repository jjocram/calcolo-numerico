function [ x, iter, vscarti ] = sor( A, b, x0, itmax, tol, omega)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    
    %vettore iniziale x_0
    x_old = x0;
    
    %Partizionamento della matrice
    D = diag(diag(A));
    E = -tril(A, -1);
    F = -triu(A, +1);
    
    M = D - omega*E;
    N = (1-omega)*D+omega*F;
    
    %Vettore contente le norme degli scarti
    vscarti= [];
    scarto = 1;
    iter = 0;
    
    %Ciclo iterativo M x_(k+1) = N x_(k) + b
    while scarto > tol && iter < itmax
       iter = iter + 1;
       termine_noto = N*x_old + omega*b;
       x = M\termine_noto; %Notare il BACK SLASH (che permette la risoluzuione di un sistema (matrix left division)) e NON lo SLASH normale (che vuol diure divisione) 
       scarto = norm(x - x_old);
       vscarti = [vscarti; scarto];
       x_old = x;
    end

end

