function [x, iter, vscarti] = gaus_seidel(A, b, x0, itmax, tol)
    %Metodo iterativo di Gaus-Seidel
    %Uso: [x, iter, vscarti]=gaus_seidel(A, b, x0, itmax, tol)
    
    %vettore iniziale x_0
    x_old = x0;
    
    %Partizionamento della matrice
    D = diag(diag(A));
    E = -tril(A, -1);
    F = -triu(A, +1);
    M = E+D;
    N = -F;
    
    %Vettore contente le norme degli scarti
    vscarti= [];
    scarto = 1;
    iter = 0;
    
    %Ciclo iterativo Mx_(k+1) = Nx_(k) + b
    while scarto > tol && iter < itmax
       iter = iter + 1;
       tnoto = N*x_old + b;
       x = M\tnoto; %Notare il BACK SLASH (che permette la risoluzuione di un sistema (matrix left division)) e NON lo SLASH normale (che vuol diure divisione) 
       scarto = norm(x - x_old);
       vscarti = [vscarti; scarto];
       x_old = x;
    end
end