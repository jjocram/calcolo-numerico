function [x, iter, vscarti] = jacobi(A, b, x0, itmax, tol)
    %Metodo iterativo di Jacobi
    %Uso: [x, iter, vscarti]=jacobi(A, b, x0, itmax, tol)
    
    %vettore iniziale x_0
    x_old = x0;
    
    %Partizionamento della matrice
    D = diag(diag(A));
    E = -tril(A, -1);
    F = -triu(A, +1);
    M = D;
    N = (E+F);
    
    %Vettore contente le norme degli scarti
    vscarti= [];
    scarto = 1;
    iter = 0;
    
    %Ciclo iterativo M x_(k+1) = N x_(k) + b
    while scarto > tol && iter < itmax
       iter = iter + 1;
       tnoto = N*x_old + b;
       x = M\tnoto; %Notare il BACK SLASH (che permette la risoluzuione di un sistema (matrix left division)) e NON lo SLASH normale (che vuol diure divisione) 
       scarto = norm(x - x_old);
       vscarti = [vscarti; scarto];
       x_old = x;
    end
end

%Nota bene: il back slash (\) permette di risolvere sistemi di equazioni
%raprresentati come matrice
% \ := Solve systems of linear equations Ax = B for x
% Uso: x = A\B